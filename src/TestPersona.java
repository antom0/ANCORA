import java.util.Scanner;
import java.util.*;
import java.io.*;
import java.lang.*;

public class TestPersona {
	public static void main(String []args) {

	Scanner intero = new Scanner(System.in);
	Scanner stringa = new Scanner(System.in);

	String nome;
	String cognome;
	int age;
	String genere;

	System.out.println("Crea la tua persona: ");


	System.out.println("Inserisci nome: ");
	nome = stringa.nextLine();
	System.out.println("Inserisci cognome: ");
	cognome = stringa.nextLine();
	System.out.println("Inserisci età: ");
	age = intero.nextInt();
	System.out.println("Inserisci genere: ");
	genere = stringa.nextLine();

	intero.close();
	stringa.close();
	
	Persona a = new Persona(nome,cognome,age,genere);
	System.out.println(a.toString());
	}
}
