import java.lang.*;

public class Persona {
	private String nome;
	private String cognome;
	private int age;
	private String genere;
	
	public Persona(String nome, String cognome, int age, String genere) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.age = age;
		this.genere = genere;
	}

	public Persona() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	@Override
	public String toString() {
		return "Persona [nome=" + nome + ", cognome=" + cognome + ", age=" + age + ", genere=" + genere + "]";
	}
	
}
